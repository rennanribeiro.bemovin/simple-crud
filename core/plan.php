<?php
require_once 'connection.php';

/**
* Trata interações com o recurso plans
*/
class Plan
{
  private $conn;

  function __construct()
  {
    $this->conn = new DBConnection();

    if(isset($_POST['create']))
      $this->create();
    if(isset($_POST['update']))
      $this->update();
    elseif($_GET['excluir'])
      $this->delete();

    if (isset($_POST['create']) || isset($_POST['update']) || $_GET['excluir'])
      return $this->redirect_to('/plans/index'); 
  }

  private function create()
  {
    $this->conn->execute(
      "INSERT INTO plans (name, ans, cnpj, status) VALUES ('{$_POST['name']}', '{$_POST['ans']}', '{$_POST['cnpj']}', '{$_POST['status']}')"
    );
  }

  private function update()
  {
    $this->conn->execute("UPDATE plans SET name = '{$_POST['name']}', ans = '{$_POST['ans']}', cnpj = '{$_POST['cnpj']}', status = '{$_POST['status']}' WHERE id = '{$_POST['id']}'");
  }

  private function delete()
  {
    $this->conn->execute("DELETE FROM plans WHERE id = '{$_GET['excluir']}'");
  }

  private function redirect_to($path)
  {
    header('Location: ' . $path);
  }

  static function find_all()
  {
    $db = new DBConnection();
    $data = array();

    if($result = $db->execute("SELECT * FROM plans ORDER BY id DESC"))
      while($row = $result->fetch_assoc())
        $data[] = $row;

    return $data;
  }

  static function find($id)
  {
    $db = new DBConnection();
    return $db->execute("SELECT * FROM plans WHERE id = '$id'")->fetch_assoc();
  }

}

new Plan();