<?php 
/**
*  Class de connexao com o banco mysql
*/
class DBConnection
{
  public $_conn;

  function __construct()
  {
    $this->conn = new mysqli('mysql', 'root', 'root', 'hospital');
    return $this->conn;
  }

  public function execute($sql)
  {
    return $this->conn->query($sql);
  }
}