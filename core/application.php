<?php

/**
* Boot da apliaction
*/

class Application
{
  
  function __construct()
  {
    if ($this->is_core())
      include self::get_core_path();
    else
      include $this->get_layout_path();
  }

  public function is_core()
  {
    return strrpos($_SERVER['REQUEST_URI'], 'core') !== false;
  }

  private static function _yield()
  {
    $view = self::get_view_path();
    
      if(!file_exists($view))
          $view = self::get_not_found_path();

    include $view;
  }

  private static function get_layout_path()
  {
    return "views/layout/application.php";
  }

  private static function get_not_found_path()
  {
    return "views/errors/404.php";
  }

  private static function get_view_path()
  {
    $view_data = explode('?', $_SERVER['REQUEST_URI']);
    return "views/{$view_data[0]}.php";
  }

  private static function get_core_path()
  {
    $core_data = explode('?', $_SERVER['REQUEST_URI']);
    return substr($core_data[0], 1) . '.php';
  }

}