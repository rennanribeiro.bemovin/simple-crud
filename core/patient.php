<?php
require_once 'connection.php';

/**
* Trata interações com o recurso plans
*/
class Patient
{
  private $conn;

  function __construct()
  {
    $this->conn = new DBConnection();

    if(isset($_POST['create']))
      $this->create();
    if(isset($_POST['update']))
      $this->update();
    elseif($_GET['excluir'])
      $this->delete();

    if (isset($_POST['create']) || isset($_POST['update']) || $_GET['excluir'])
      return $this->redirect_to('/patients/index'); 
  }

  private function create()
  {
    $this->conn->execute(
      "INSERT INTO patients (name, email, birth_date, status, address, plans_id) VALUES ('{$_POST['name']}', '{$_POST['email']}', '{$_POST['birth_date']}', '{$_POST['status']}', '{$_POST['address']}', '{$_POST['plan_id']}')"

    );
  }

  private function update()
  {
    $this->conn->execute("UPDATE patients SET name = '{$_POST['name']}', ans = '{$_POST['ans']}', cnpj = '{$_POST['cnpj']}', status = '{$_POST['status']}' WHERE id = '{$_POST['id']}'");
  }

  private function delete()
  {
    $this->conn->execute("DELETE FROM patients WHERE id = '{$_GET['excluir']}'");
  }

  private function redirect_to($path)
  {
    header('Location: ' . $path);
  }

  static function find_all()
  {
    $db = new DBConnection();
    $data = array();

    if($result = $db->execute("SELECT pt.*, pl.name as plan_name FROM patients pt JOIN plans pl ON pt.`plans_id` = pl.id"))
      while($row = $result->fetch_assoc())
        $data[] = $row;

    return $data;
  }

  static function find($id)
  {
    $db = new DBConnection();
    return $db->execute("SELECT pt.*, pl.id as _plan_id FROM patients pt JOIN plans pl ON pt.`plans_id` = pl.id WHERE pt.id = '$id'")->fetch_assoc();
  }

}

new Patient();