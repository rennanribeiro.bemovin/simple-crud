<?php 
  include 'core/patient.php';
  include 'core/plan.php';
  
  $plans = Plan::find_all();
  $patient = Patient::find($_GET['id']);
?>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Editar o paciente: <?php echo $patient['name'] ?></h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
      <form role="form" method="POST" name="patient" action="/core/patient">
        <input type="hidden" name="id" value="<?php echo $patient['id'] ?>">
        <div class="form-group">
            <label>Nome</label>
            <input name="name" value="<?php echo $patient['name'] ?>" class="form-control">
        </div>
        <div class="form-group">
          <label>E-mail</label>
          <input type="email" name="email" value="<?php echo $patient['email'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Data de Nascimento</label>
            <input name="birth_date" value="<?php echo $patient['birth_date'] ?>" type="date" class="form-control">
        </div>
        <div class="form-group">
            <label>Endereço Completo</label>
            <input name="address" value="<?php echo $patient['address'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Plano</label>
            <select name="plan_id" class="form-control">
            <?php foreach ($plans as $plan): ?>    
                <option <?php echo ($patient['_plan_id'] == $plan['id']) ? 'selected="selected"' : '' ?>" value="<?php echo $plan['id']  ?>"><?php echo $plan['name']  ?></option>
            <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <div class="checkbox">
              <input type="hidden" name="status" value="0">
              <label><input name="status" type="checkbox" <?php echo ($patient['status'] == 1) ? 'checked="checked"' : '' ?>" value="1">Ativo?</label>
            </div>
        </div>
        <button type="submit" name="update" value="update" class="btn btn-primary">Editar Paciente</button>
      </form>
  </div>
</div>