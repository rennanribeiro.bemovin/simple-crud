<?php 
  include 'core/plan.php';
  $plans = Plan::find_all();
?>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Novo Paciente</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
      <form role="form" method="POST" action="/core/patient">
          <div class="form-group">
              <label>Nome</label>
              <input name="name" class="form-control">
          </div>
          <div class="form-group">
              <label>E-mail</label>
              <input type="email" name="email" class="form-control">
          </div>
          <div class="form-group">
              <label>Data de Nascimento</label>
              <input name="birth_date" type="date" class="form-control">
          </div>
          <div class="form-group">
              <label>Endereço Completo</label>
              <input name="address" class="form-control">
          </div>
          <div class="form-group">
              <label>Plano</label>
              <select name="plan_id" class="form-control">
                <option selected="selected">Selecione um plano</option>
              <?php foreach ($plans as $plan): ?>    
                  <option value="<?php echo $plan['id']  ?>"><?php echo $plan['name']  ?></option>
              <?php endforeach ?>
              </select>
          </div>
          
          <div class="form-group">
              <div class="checkbox">
                <input type="hidden" name="status" value="0">
                <label><input name="status" type="checkbox" value="1">Ativo?</label>
              </div>
          </div>
          <button type="submit" name="create" value="create" class="btn btn-primary">Cadastrar Paciente</button>
      </form>
  </div>
</div>