<?php 
  include 'core/patient.php';
  
  $patients = Patient::find_all();
?>
<div class="row">
    <div class="col-lg-12"><h1 class="page-header">Lista de Planos</h1></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Data Nascimento</th>
                        <th>Endereço</th>
                        <th>Plano</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($patients as $patient): ?>    
                    <tr>
                        <td><?php echo $patient['name'] ?></td>
                        <td><?php echo $patient['email'] ?></td>
                        <td><?php echo $patient['birth_date'] ?></td>
                        <td><?php echo $patient['address'] ?></td>
                        <td><?php echo $patient['plan_name'] ?></td>
                        <td><?php echo ($patient['status'] == 1) ? 'Ativo' : 'Inativo' ?></td>
                        <td>
                            <a href="/patients/show?id=<?php echo $patient['id'] ?>" class="btn btn-primary btn-block">Ver</a>
                            <a href="/patients/update?id=<?php echo $patient['id'] ?>" class="btn btn-default btn-block" >Editar</a>
                            <a href="/core/patient?excluir=<?php echo $patient['id'] ?>" class="btn btn-default btn-danger btn-block" >Excluir</a>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
