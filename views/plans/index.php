<?php include 'core/plan.php' ?>
<div class="row">
    <div class="col-lg-12"><h1 class="page-header">Lista de Planos</h1></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Registro ANS</th>
                        <th>CNPJ</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php $plans = Plan::find_all(); ?>
                <?php foreach ($plans as $plan): ?>    
                    <tr>
                        <td><?php echo $plan['name'] ?></td>
                        <td><?php echo $plan['ans'] ?></td>
                        <td><?php echo $plan['cnpj'] ?></td>
                        <td><?php echo ($plan['status'] == 1) ? 'Ativo' : 'Inativo' ?></td>
                        <td>
                            <a href="/plans/show?id=<?php echo $plan['id'] ?>" class="btn btn-primary">Ver</a>
                             - 
                            <a href="/plans/update?id=<?php echo $plan['id'] ?>" class="btn btn-default " >Editar</a>
                            - 
                            <a href="/core/plan?excluir=<?php echo $plan['id'] ?>" class="btn btn-default btn-danger" >Excluir</a>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
