<?php 
  include 'core/plan.php';
  $plan = Plan::find($_GET['id']);
?>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Editar o plano: <?php echo $plan['name'] ?></h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
      <form role="form" method="POST" name="plan" action="/core/plan">
        <input type="hidden" name="id" value="<?php echo $plan['id'] ?>">
        <div class="form-group">
            <label>Nome</label>
            <input name="name" value="<?php echo $plan['name'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Registro do ANS</label>
            <input name="ans" value="<?php echo $plan['ans'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>CNPJ</label>
            <input name="cnpj" value="<?php echo $plan['cnpj'] ?>" class="form-control">
        </div>
        <div class="form-group">
            <div class="checkbox">
              <input type="hidden" name="status" value="0">
              <label><input name="status" type="checkbox" <?php echo ($plan['status'] == 1) ? 'checked="checked"' : '' ?>" value="1">Ativo?</label>
            </div>
        </div>
        <button type="submit" name="update" value="update" class="btn btn-primary">Editar Plano</button>
      </form>
  </div>
</div>