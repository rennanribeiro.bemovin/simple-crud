<?php 
  include 'core/plan.php';
  $plan = Plan::find($_GET['id']);
?>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Plano <?php echo $plan['name'] ?></h1>
  </div>
  <div class="col-lg-5">
    <strong>ID: </strong> #<?php echo $plan['id'] ?> <br/>
    <strong>Nome: </strong> <?php echo $plan['name'] ?> <br/>
    <strong>Registro ANS: </strong> <?php echo $plan['ans'] ?> <br/>
    <strong>CNPJ: </strong> <?php echo $plan['cnpj'] ?> <br/>
    <strong>Status: </strong> <?php echo $plan['status'] == 1 ? 'Ativo' : 'Inativo' ?> <br/>
  </div>
</div>
