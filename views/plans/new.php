<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Novo Plano</h1>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
      <form role="form" method="POST" name="plan" action="/core/plan">
          <div class="form-group">
              <label>Nome</label>
              <input name="name" class="form-control">
          </div>
          <div class="form-group">
              <label>Registro do ANS</label>
              <input name="ans" class="form-control">
          </div>
          <div class="form-group">
              <label>CNPJ</label>
              <input name="cnpj" class="form-control">
          </div>
          <div class="form-group">
              <div class="checkbox">
                <input type="hidden" name="status" value="0">
                <label><input name="status" type="checkbox" value="1">Ativo?</label>
              </div>
          </div>
          <button type="submit" name="create" value="create" class="btn btn-primary">Cadastrar Plano</button>
      </form>
  </div>
</div>